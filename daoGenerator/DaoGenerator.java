
package solutions.chor.moviedb.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class MoviedbDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1000, "solutions.chor.moviedb");

        addCache(schema);        

        new DaoGenerator().generateAll(schema, "../app/src/main/java/dao");
    }

    private static void addNote(Schema schema) {
        Entity note = schema.addEntity("Note");
        note.addIdProperty();
        note.addStringProperty("query").notNull();
        note.addStringProperty("searchResult").notNull();
    
    }

    

}