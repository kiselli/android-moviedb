
package chor.solutions.moviedb.dao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class MoviedbDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1000, "chor.solutions.moviedb.dao");

        addCache(schema);                

        new DaoGenerator().generateAll(schema, "../app/src/main/java/");
    }

    private static void addCache(Schema schema) {
        Entity note = schema.addEntity("SearchResult");
        note.addIdProperty();
        note.addStringProperty("query").notNull();
        note.addStringProperty("searchResult").notNull();
    
    }

    

}