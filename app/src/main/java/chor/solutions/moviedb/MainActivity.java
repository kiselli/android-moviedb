package chor.solutions.moviedb;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.gson.Gson;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;


import chor.solutions.moviedb.fragments.CacheFragment;

import chor.solutions.moviedb.fragments.CacheFragment_;
import chor.solutions.moviedb.fragments.SearchFragment_;
import chor.solutions.moviedb.funcs.Func1;
import chor.solutions.moviedb.model.Movie;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.main_menu)
public class MainActivity extends ActionBarActivity {


    @ViewById(R.id.main_fragment_wrapper)
    FrameLayout fragmentWrapper;

//    @OptionsMenuItem
//    MenuItem menuCache;

    @OptionsItem(R.id.menuCache)
    void menuCacheSelected(){
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.main_fragment_wrapper, new CacheFragment_())
            .addToBackStack("cache")
            .commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // load init fragment
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_fragment_wrapper, new SearchFragment_())
                .commit();

    }






}
