package chor.solutions.moviedb.funcs;

/**
 * Created by oliver on 04/11/2015.
 */
public interface Func1<T> {
    public void execute(T obj);
}
