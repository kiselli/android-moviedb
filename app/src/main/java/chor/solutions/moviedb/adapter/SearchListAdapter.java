package chor.solutions.moviedb.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.ViewById;

import chor.solutions.moviedb.dao.SearchResult;
import chor.solutions.moviedb.model.Movie;

/**
 * Created by oliver on 05/11/2015.
 */
@EBean
public class SearchListAdapter extends ArrayAdapter<SearchResult> {
    public SearchListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position,convertView,parent);

        SearchResult result = getItem(position);

        TextView text = (TextView)view.findViewById(android.R.id.text1);
        text.setText(result.getQuery());

        return view;


    }
}

