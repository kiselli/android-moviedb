package chor.solutions.moviedb.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import org.androidannotations.annotations.EBean;

import chor.solutions.moviedb.model.Movie;

/**
 * Created by oliver on 05/11/2015.
 */
@EBean
public class MovieListAdapter extends ArrayAdapter<Movie> {
    public MovieListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
    }
}
