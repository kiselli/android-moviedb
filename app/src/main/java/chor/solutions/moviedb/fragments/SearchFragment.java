package chor.solutions.moviedb.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import chor.solutions.moviedb.HttpCache;
import chor.solutions.moviedb.MainActivity;
import chor.solutions.moviedb.R;
import chor.solutions.moviedb.adapter.MovieListAdapter;
import chor.solutions.moviedb.model.Movie;

/**
 * Created by oliver on 13/10/2015.
 */
@EFragment(R.layout.search_fragment)
public class SearchFragment extends Fragment {

    @Bean
    HttpCache httpCache;

    RequestQueue queue;
    Gson gson;

    @ViewById(R.id.search_input)
    EditText searchInput;
    @ViewById(R.id.listView)
    ListView listView;


    @Bean(MovieListAdapter.class)
    ArrayAdapter<Movie> listAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        queue = Volley.newRequestQueue(getActivity());
        gson = new Gson();

    }

    @AfterViews
    void bindAdapter() {
        listView.setAdapter(listAdapter);
    }

    @ItemClick
    void listViewItemClicked(Movie movie){
        SearchResultFragment searchResultFragment = new SearchResultFragment_();
        searchResultFragment.setMovie(movie);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_wrapper, searchResultFragment)
                .addToBackStack("search_result")
                .commit();
    }

    @TextChange(R.id.search_input)
    void onTextChangesOnHelloTextView(CharSequence text, TextView hello, int before, int start, int count) {
        // Something Here
        if(count > 1){
            startSearch(text.toString());
        }
    }

    private void startSearch(String query){

        queue.cancelAll(true);

        String cached = httpCache.findByQuery(query);
        if(cached != null){
            parseSearchResult(cached);
        }
        else{
            startSearchRequest(query);
        }

    }

    private void startSearchRequest(final String query){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, buildUrl(query),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        parseSearchResult(response);
                        httpCache.saveSearchResult(query,response);
                        //listAdapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        error.printStackTrace();
                    }
                });

        queue.add(stringRequest);
    }

    private void parseSearchResult(String searchResult){
        Movie movie = gson.fromJson(searchResult, Movie.class);
        listAdapter.clear();
        listAdapter.add(movie);
    }



    private String buildUrl(String query){

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("www.omdbapi.com")
                .appendQueryParameter("type", "movie")
                .appendQueryParameter("plot", "full")
                .appendQueryParameter("r", "json")
                .appendQueryParameter("t", query);

        return builder.build().toString();

    }
}
