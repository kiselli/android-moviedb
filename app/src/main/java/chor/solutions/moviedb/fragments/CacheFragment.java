package chor.solutions.moviedb.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import chor.solutions.moviedb.HttpCache;
import chor.solutions.moviedb.R;
import chor.solutions.moviedb.adapter.SearchListAdapter;
import chor.solutions.moviedb.dao.SearchResult;
import chor.solutions.moviedb.model.Movie;

/**
 * Created by oliver on 05/11/2015.
 */
@EFragment(R.layout.cache_fragment)
public class CacheFragment extends Fragment {

    @Bean(SearchListAdapter.class)
    ArrayAdapter<SearchResult> listAdapter;

    @Bean
    HttpCache httpCache;

    @ViewById(R.id.cacheList)
    ListView listView;

    @AfterViews
    void bindAdapter() {
        listView.setAdapter(listAdapter);

        listAdapter.addAll(httpCache.findAllResults());
    }

    @ItemClick
    void cacheListItemClicked(SearchResult result){
        Movie movie = new Gson().fromJson(result.getSearchResult(),Movie.class);

        if(movie == null || movie.getTitle() == null){
            new AlertDialog.Builder(getActivity())
                    .setTitle("No movie for this Query!")
                    .setMessage("OMDB hast no Movie for this query?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }else {
            SearchResultFragment searchResultFragment = new SearchResultFragment_();
            searchResultFragment.setMovie(movie);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_fragment_wrapper, searchResultFragment)
                    .addToBackStack("search_result")
                    .commit();
        }
    }
}
