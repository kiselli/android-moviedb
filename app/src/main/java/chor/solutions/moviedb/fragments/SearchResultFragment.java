package chor.solutions.moviedb.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

import chor.solutions.moviedb.R;
import chor.solutions.moviedb.model.Movie;

/**
 * Created by oliver on 04/11/2015.
 */
@EFragment(R.layout.search_result_fragment)
public class SearchResultFragment extends Fragment {

    //Search results should at least show the title of the movie, the detail page should at least show the
    //title, the year of release, the runtime and the poster

    @ViewById(R.id.search_result_title)
    TextView titleText;
    @ViewById(R.id.search_result_year)
    TextView yearText;
    @ViewById(R.id.search_result_duration)
    TextView durationText;
    @ViewById(R.id.search_result_poster)
    ImageView posterImage;
    @ViewById(R.id.search_result_plot)
    TextView plotText;

    private Movie movie;


    @AfterViews
    void updateView() {
        if(movie != null){
            setMovie(movie);
        }
    }

    public void setMovie(Movie movie){
        this.movie = movie;

        // cant get around the check because of early setting stuff
        if(titleText != null) {
            titleText.setText(movie.getTitle());
        }
        if(yearText != null) {
            yearText.setText(movie.getReleased());
        }
        if(durationText != null){
            durationText.setText(movie.getRuntime());
        }
        if(posterImage != null){
            Picasso.with(getActivity()).load(movie.getPoster()).into(posterImage);
        }
        if(plotText != null){
            plotText.setText(movie.getPlot());
        }

    }



}
