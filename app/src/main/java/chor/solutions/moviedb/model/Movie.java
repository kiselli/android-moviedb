package chor.solutions.moviedb.model;

/**
 * Created by oliver on 04/11/2015.
 */
public class Movie {

    private String Title;
    private String Poster;
    private String Released;
    private String Runtime;
//    private String Genre;
//    private String Director;
//    private String Writer;
//    private String Actors;
    private String Plot;
//    private String Language;
//    private String Country;
//    private String Awards;
//    private String Metascore;
//    private String imdbRating;
//    private String imdbVotes;
//    private String imdbID;
//    private String Type;
//    private String Response;


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }

    public String getReleased() {
        return Released;
    }

    public void setReleased(String released) {
        Released = released;
    }

    public String getRuntime() {
        return Runtime;
    }

    public void setRuntime(String runtime) {
        Runtime = runtime;
    }

    public String getPlot() {
        return Plot;
    }

    public void setPlot(String plot) {
        Plot = plot;
    }

    @Override
    public String toString() {
        return Title;
    }
}
