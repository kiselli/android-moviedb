package chor.solutions.moviedb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import chor.solutions.moviedb.dao.DaoMaster;
import chor.solutions.moviedb.dao.DaoSession;
import chor.solutions.moviedb.dao.SearchResult;
import chor.solutions.moviedb.dao.SearchResultDao;

/**
 * Created by oliver on 05/11/2015.
 */
@EBean(scope = EBean.Scope.Singleton)
public class HttpCache {

    DaoMaster daoMaster;
    private SQLiteDatabase db;
    private DaoSession daoSession;
    private SearchResultDao searchResultDao;



    public HttpCache(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "movie-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        searchResultDao = daoSession.getSearchResultDao();

    }

    public String findByQuery(String query){
        List<SearchResult> searchResults = searchResultDao.queryBuilder().where(SearchResultDao.Properties.Query.eq(query)).list();
        if(searchResults.size() < 1){
            return null;
        }
        return searchResults.get(0).getSearchResult();
    }

    public void saveSearchResult(String query, String searchResult){
        SearchResult entity = new SearchResult(null, query, searchResult);
        searchResultDao.insert(entity);
    }

    public List<SearchResult> findAllResults(){
        return searchResultDao.queryBuilder().build().list();

    }
}
